//
//  MobFirstGeoFencing.h
//  MobFirstGeoFencing
//
//  Created by Diego P Navarro on 18/10/16.
//  Copyright © 2016 Diego P Navarro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <UIKit/UIKit.h>

#import "MobFirstGeoFencingStorage.h"
#import "MobFirstGeofencingConnection.h"

@interface MobFirstGeoFencing : NSObject <CLLocationManagerDelegate>

/*
 * Get Service Instance
 */

+(MobFirstGeoFencing*) getInstance;


/*
 * Get LocationManager Instance
 */


+(CLLocationManager *)locationManager;




/*
 * Start Services
 */

-(void) startService;



/*
 * CHECK IF LAUNCHER APP IS FROM APP
 */

-(void) checkIfApplicationStartFromGeofencingNotification:(NSDictionary *)launchOptions;


/*
 * HANDLER RECEIVE LOCAL NOTIFICATION
 */

-(void) onReceiveLocalNotification:(UIApplication *)application notification:(UILocalNotification *)notification;


/*
 * HANDLER APP BECOME ACTIVE
 */

-(void) checkIfApplicationActiveFromGeofencingNotification;



@end
