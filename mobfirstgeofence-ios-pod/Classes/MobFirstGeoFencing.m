//
//  MobFirstGeoFencing.m
//  MobFirstGeoFencing
//
//  Created by Diego P Navarro on 18/10/16.
//  Copyright © 2016 Diego P Navarro. All rights reserved.
//

#import "MobFirstGeoFencing.h"
#import "AppPOI.h"
#import "MobFirstGeoFencingUtils.h"

/*
 * Update Time 60 seconds
 */

#define updateTime 60



/*
 * Keep single instance from services
 */

static MobFirstGeoFencing* instance;
static CLLocationManager* locationManager;
static CLLocationManager* locationSignificantManager;
static CLLocation * betterLocation;


@interface MobFirstGeoFencing(){
    UIBackgroundTaskIdentifier backgroundTaskIdentifier;
}

@end


@implementation MobFirstGeoFencing



/*
 * Get Service Instance
 */

+(MobFirstGeoFencing*) getInstance{
    
    if(!instance){
        instance = [MobFirstGeoFencing new];
    }
    
    return instance;
}



/*
 * Get LocationManager Instance
 */


+ (CLLocationManager *)locationManager {
    @synchronized(self) {
        if (locationManager == nil) {
            locationManager = [[CLLocationManager alloc] init];
            locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
        }
    }
    return locationManager;
}


+ (CLLocationManager *)locationSignificantManager {
    @synchronized(self) {
        if (locationSignificantManager == nil) {
            locationSignificantManager = [[CLLocationManager alloc] init];
        }
    }
    return locationManager;
}



-(CLLocationManager *)setStartLocationManager{
    
    CLLocationManager *locationManager = [MobFirstGeoFencing locationManager];
    
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    
    [locationManager requestAlwaysAuthorization];
    [locationManager startUpdatingLocation];
    
    return locationManager;
}

-(CLLocationManager *)setStartSignificantLocationManager{
    
    CLLocationManager *locationManager = [MobFirstGeoFencing locationSignificantManager];
    
    locationManager.delegate = self;
    [locationManager requestAlwaysAuthorization];
    [locationManager startMonitoringSignificantLocationChanges];
    
    return locationManager;
}



/*
 * Start Services
 */

-(void) startService{

    
    if([[UIApplication sharedApplication] backgroundRefreshStatus] == UIBackgroundRefreshStatusDenied
       || [[UIApplication sharedApplication] backgroundRefreshStatus] == UIBackgroundRefreshStatusRestricted){
        
    
        /*
         * Service not started
         */
        
    
    } else{
        
        
        /*
         * POIs Update
         */
        
        [MobFirstGeofencingConnection startUpdatePOIs];
        
        
        
        /*
         * Location Update
         */
        
        [self startLocationService];
        
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationEnterBackground) name:UIApplicationDidEnterBackgroundNotification object:nil];
        
        
    }
}


-(void)applicationEnterBackground{
    
    
    /*
     * Background task on the main thread
     */
     dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        
        /*
         * Location Update
         */
        
        
        [self startLocationService];
        
        /*
         * POIs Update
         */
        
        [MobFirstGeofencingConnection startUpdatePOIs];
        
    });
    
    backgroundTaskIdentifier = UIBackgroundTaskInvalid;
    
    UIApplication *app = [UIApplication sharedApplication];
    
    if ([app respondsToSelector:@selector(beginBackgroundTaskWithExpirationHandler:)])
    {
        backgroundTaskIdentifier = [app beginBackgroundTaskWithExpirationHandler:^
                   {
                       dispatch_async(dispatch_get_main_queue(), ^
                                      {
                                          if (backgroundTaskIdentifier != UIBackgroundTaskInvalid)
                                          {
                                              /*
                                               * Time Over
                                               */
                                              [app endBackgroundTask:backgroundTaskIdentifier];
                                              backgroundTaskIdentifier = UIBackgroundTaskInvalid;
                                          }
                                      });
                   }];
    }
}


-(void) startLocationService{
    
    [self setStartLocationManager];
    [self setStartSignificantLocationManager];
    [NSObject cancelPreviousPerformRequestsWithTarget: self selector:@selector(updateLocation) object: self];
    [self performSelector:@selector(updateLocation) withObject: self afterDelay: updateTime];
    
}

-(void) updateLocation{
    [self startLocationService];
    
    if(betterLocation != nil){
        [self checkLocationNearFromPOI:betterLocation.coordinate];
    }
}

#pragma mark - CLLocationManagerDelegate Methods

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    
    if(manager == locationSignificantManager){
        [self checkLocationNearFromPOI:((CLLocation*)[locations lastObject]).coordinate];
    }
    else{
        if(betterLocation != nil){
            [self checkLocationNearFromPOI:((CLLocation*)[locations lastObject]).coordinate];
        }
        betterLocation = [locations lastObject];
    }
}

#pragma MobFirst GeoFencing Methods

-(void) checkLocationNearFromPOI:(CLLocationCoordinate2D) coordinate{
    
    NSArray* listPoi = [MobFirstGeoFencingStorage getPois];
    
    if(!listPoi || [listPoi count] == 0){
        return;
    }
    
    
    NSMutableArray* activedNotifications = [NSMutableArray new];
    
    
    for(AppPOI* poi in listPoi){
        if(poi.notifications){
            for(AppPOINotification* notification in poi.notifications){
                if([self isPoiNotificationActived:notification andUserLocation:coordinate]){
                    [activedNotifications addObject:notification];
                }
            }
        }
    }

    if([activedNotifications count] == 0){
        return;
    }
    
    
    AppPOINotification* notificationItem = nil;
    
    for(AppPOINotification* notification in activedNotifications){
        if(!notificationItem || notificationItem.distance > notification.distance){
            notificationItem = notification;
        }
    }
    
    
    if(!notificationItem){
        return;
    }
    
    [MobFirstGeofencingConnection setUsageNotificationTrigger:[NSString stringWithFormat:@"%d",notificationItem.id]];
    
    [MobFirstGeoFencingStorage saveNotification:notificationItem andDateTrigger: [NSDate date]];

    UILocalNotification* localNotification = [[UILocalNotification alloc] init];    
    localNotification.timeZone = [NSTimeZone localTimeZone];
    localNotification.alertBody = notificationItem.shortMessage;
    localNotification.soundName = UILocalNotificationDefaultSoundName;
    localNotification.applicationIconBadgeNumber =  [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
    localNotification.userInfo = @{
                                   notificationIdKey : [NSNumber numberWithInt:notificationItem.id],
                                   poiIdKey : [NSNumber numberWithInt:notificationItem.poiId]
                                   };
    
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
}



-(BOOL) isPoiNotificationActived:(AppPOINotification*) notification andUserLocation:(CLLocationCoordinate2D) coordinate{
    
    
    /**
     *  Check if notification is active
     * */
    
    
    if(!notification.active){
        return NO;
    }
    
    
    
    /**
     *  Check if notification is active
     * */
    
    
    NSDate* startDate = [MobFirstGeoFencingUtils convertStringToDate:notification.startDate];
    NSDate* endDate = [MobFirstGeoFencingUtils convertStringToDate:notification.endDate];
    
    if(startDate && endDate){
        
        NSDate* now = [NSDate date];
        
        if(!([startDate earlierDate: now] == startDate && [now earlierDate: endDate] == now)){
            
            
            /**
             *  Over period date
             * */
            
            return NO;
            
        }
    }
    

    double distance = [MobFirstGeoFencingUtils convertGeoLocDistanceOnMeters:(double)coordinate.latitude longitude:(double)coordinate.longitude latitudeCompare:notification.latitude longitudeCompare:notification.longitude];
    
    
    
    /*
     * set distance on notification
     * */
    
    
    if(distance > notification.radius){
        return NO;
    }
    
    
    notification.distance = distance;
    
    AppPOINotification* savedNotification = [MobFirstGeoFencingStorage getNotification:notification.id andPOIId:notification.poiId];
    
    if(!savedNotification){
        
        /**
         * Not shot yet
         * */
        
        return YES;
    }

    
    NSDate * lastTriggerDate = [MobFirstGeoFencingUtils convertStringToDate:savedNotification.lastTriggerDate];
    
    
    NSTimeInterval secondsBetween = [[NSDate date] timeIntervalSinceDate:lastTriggerDate];
    
    /*
     * timeIntervalSinceDate return the value in seconds, so dividing by 3600 get time in hour
     */
    
    int hourbetweenTriggerAndNow = secondsBetween/3600;
    
    if( hourbetweenTriggerAndNow < notification.triggerInterval){
        
        /**
         * Not past yet
         * */
        
        return NO;
    }
    
    
    if(!notification.hasTriggerPeriod ||
        ![notification triggerPeriodEndtTime] ||
        ![notification triggerPeriodStartTime]){
        
        /*
         * Can use
         */
        
        return YES;
        
    }
    
    
    
    @try {
        
        NSString* triggerPeriodStartTime = [notification triggerPeriodStartTime];
        
        NSArray* triggerPeriodStartTimeComponents = [triggerPeriodStartTime componentsSeparatedByString:@":"];
        
        if([triggerPeriodStartTimeComponents count] > 1){
            
            int hourStart = [triggerPeriodStartTimeComponents[0] intValue];
            int minuteStart = [triggerPeriodStartTimeComponents[1] intValue];
     
            NSDate *date = [NSDate date];
            NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier: NSGregorianCalendar];
        
            NSDateComponents *components = [gregorian components: NSUIntegerMax fromDate: date];
            
            [components setHour: hourStart];
            [components setMinute: minuteStart];
             
            NSDate *dateStartPeriod = [gregorian dateFromComponents: components];
     
            
            NSString* triggerPeriodEndTime = [notification triggerPeriodEndtTime];
            
            NSArray* triggerPeriodEndTimeComponents = [triggerPeriodEndTime componentsSeparatedByString:@":"];
            
            if([triggerPeriodEndTimeComponents count] > 1){
            
            
                int hourEnd = [triggerPeriodEndTimeComponents[0] intValue];
                int minuteEnd = [triggerPeriodEndTimeComponents[1] intValue];
                
                date = [NSDate date];
                gregorian = [[NSCalendar alloc] initWithCalendarIdentifier: NSGregorianCalendar];
                
                components = [gregorian components: NSUIntegerMax fromDate: date];
                
                [components setHour: hourEnd];
                [components setMinute: minuteEnd];
                
                NSDate *dateEndPeriod = [gregorian dateFromComponents: components];
                
                
                NSDate* now = [NSDate date];
                
                
                
                /*
                 * Check is in period
                 */
                
                
                return [dateStartPeriod earlierDate: now] == dateStartPeriod
                        && [now earlierDate: dateEndPeriod] == now;
                
                
                
            
            }
            else{
                
                /*
                 * Not contains a valid trigger period
                 */
                
                
                return YES;
                
            }
            
        }
        else{
            
            /*
             * Not contains a valid trigger period
             */
            
            
            return YES;
            
        }
   
        
        
    } @catch (NSException *exception) {
        
        /*
         * Not contains a valid trigger period
         */
        
        
        return YES;
        
        
    } @finally {}
 
}

/*
 * CHECK IF LAUNCHER APP IS FROM APP
 */

-(void) checkIfApplicationStartFromGeofencingNotification:(NSDictionary *)launchOptions{
    [MobFirstGeoFencingUtils checkIfApplicationStartFromGeofencingNotification:launchOptions];
}


/*
 * HANDLER RECEIVE LOCAL NOTIFICATION
 */

-(void) onReceiveLocalNotification:(UIApplication *)application notification:(UILocalNotification *)notification{
    [MobFirstGeoFencingUtils onReceiveLocalNotification:application notification:notification];
}


/*
 * HANDLER APP BECOME ACTIVE
 */

-(void) checkIfApplicationActiveFromGeofencingNotification{
    [MobFirstGeoFencingUtils checkIfApplicationActiveFromGeofencingNotification];
    
}


@end
