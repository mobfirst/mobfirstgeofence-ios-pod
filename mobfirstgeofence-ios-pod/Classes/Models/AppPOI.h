//
//  AppPOI.h
//  MobFirstGeoFencing
//
//  Created by Diego P Navarro on 18/10/16.
//  Copyright © 2016 Diego P Navarro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppPOI : NSObject


@property(nonatomic) int id;
@property(nonatomic, strong) NSString* title;
@property(nonatomic) double latitude;
@property(nonatomic) double longitude;
@property(nonatomic) int radius;

@property(nonatomic,strong) NSMutableArray* notifications;
@property(nonatomic,strong) NSDictionary* objectAPI;

@end
