//
//  AppPOINotification.m
//  MobFirstGeoFencing
//
//  Created by Diego P Navarro on 18/10/16.
//  Copyright © 2016 Diego P Navarro. All rights reserved.
//

#import "AppPOINotification.h"

@implementation AppPOINotification


/*
 * Validate Trigger Period
 */

-(NSString*) triggerPeriodEndtTime{
    
    if([self isValidInformation:self.triggerPeriodEndtTime]){
        return self.triggerPeriodEndtTime;
    }
    
    return nil;
}
-(NSString*) triggerPeriodStartTime{
    
    if([self isValidInformation:self.triggerPeriodStartTime]){
        return self.triggerPeriodStartTime;
    }
    
    return nil;
}



/*
 * Validate Full Message
 */

-(NSString*) getFullMessage{
    
    if([self isValidInformation:self.fullMessage]){
        return self.fullMessage;
    }
    
    return @"";
    
}



-(BOOL) isValidInformation:(NSString*) string{
    return string!= nil && ![string containsString:@"null"] && [[string stringByReplacingOccurrencesOfString:@" " withString:@""] length] > 0;
}



@end
