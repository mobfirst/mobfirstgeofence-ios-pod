//
//  AppPOINotification.h
//  MobFirstGeoFencing
//
//  Created by Diego P Navarro on 18/10/16.
//  Copyright © 2016 Diego P Navarro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppPOINotification : NSObject

@property(nonatomic) int id;
@property(nonatomic) int poiId;
@property(nonatomic) int radius;
@property(nonatomic) int triggerInterval;

@property(nonatomic, strong) NSString* shortMessage;
@property(nonatomic, strong) NSString* fullMessage;
@property(nonatomic, strong) NSString* triggerPeriodStartTime;
@property(nonatomic, strong) NSString* triggerPeriodEndtTime;
@property(nonatomic, strong) NSString* startDate;
@property(nonatomic, strong) NSString* endDate;
@property(nonatomic, strong) NSString* lastTriggerDate;

@property(nonatomic) BOOL showFullMessage;
@property(nonatomic) BOOL active;
@property(nonatomic) BOOL hasTriggerPeriod;

@property(nonatomic,strong) NSDictionary* objectAPI;

@property(nonatomic) double latitude;
@property(nonatomic) double longitude;
@property(nonatomic) double distance;



/*
 * Validate Trigger Period
 */

-(NSString*) triggerPeriodEndtTime;
-(NSString*) triggerPeriodStartTime;


/*
 * Validate Full Message
 */

-(NSString*) getFullMessage;


@end
