//
//  MobFirstAPIConstants.h
//  MobFirstGeoFencing
//
//  Created by Diego P Navarro on 29/03/17.
//  Copyright © 2017 Diego P Navarro. All rights reserved.
//

#ifndef MobFirstAPIConstants_h
#define MobFirstAPIConstants_h


#define MOBFIRST_HEADER_GRANT_TYPE @"grant_type"
#define MOBFIRST_HEADER_CLIENT_CREDENTIALS @"client_credentials"
#define MOBFIRST_HEADER_CLIENT_LOGIN @"password"
#define MOBFIRST_HEADER_CLIENT_ID @"client_id"
#define MOBFIRST_HEADER_CLIENT_SECRET @"client_secret"

/*MobFirst API*/
#define MOBFIRST_URL_BASE @"https://api.mobfirst.com/v1"
#define MOBFIRST_URL_BASE_STAGING @"https://mobfirst-api-staging.azurewebsites.net/v1"

#define MOBFIRST_COMPLEMENTION_OAUTH @"oauth/token"
#define MOBFIRST_COMPLEMENTION_APP @"apps/"
#define MOBFIRST_COMPLEMENTION_LOG @"logs"


#define HEADER_PLATFORM_NAME @"PlatformName"
#define HEADER_PLATFORM_NAME_VALUE @"iPhone"
#define HEADER_DEVICE_TOKEN @"DeviceToken"

#define MOBFIRST_HEADER_AUTHORIZATION @"Authorization"
#define ERROR_CONVERTION_CODE 1100





#endif /* MobFirstAPIConstants_h */
