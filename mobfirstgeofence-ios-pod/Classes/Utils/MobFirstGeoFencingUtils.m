//
//  MobFirstGeoFencingUtils.m
//  MobFirstGeoFencing
//
//  Created by Diego P Navarro on 18/10/16.
//  Copyright © 2016 Diego P Navarro. All rights reserved.
//

#import "MobFirstGeoFencingUtils.h"
#import "AppPOINotification.h"
#import "MobFirstGeoFencingStorage.h"
#import "MobFirstGeoFencingDialog.h"
#import "MobFirstGeofencingConnection.h"


#import <math.h>
#import <GLKit/GLKit.h>
#import <AudioToolbox/AudioServices.h>

#define toRadians(degrees)((M_PI * degrees)/180)

#define earthRadius 6378137// meters
#define dateMask @"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"

@implementation MobFirstGeoFencingUtils



/**
 *   HAVERSINE IMPLEMENTATION
 * */

+(double) convertGeoLocDistanceOnMeters:(double) latitude longitude:(double) longitude latitudeCompare:(double) latitudeCompare longitudeCompare:(double) longitudeCompare{
    
    
    double latDistance = GLKMathDegreesToRadians(latitudeCompare - latitude);
    double longDistance = GLKMathDegreesToRadians(longitudeCompare - longitude);
 
    latitude = GLKMathDegreesToRadians(latitude);
    latitudeCompare = GLKMathDegreesToRadians(latitudeCompare);
 
    double value = sin(latDistance/2) * sin(latDistance/2) + cos(latitude) * cos(latitudeCompare)*
                            sin(longDistance/2)* sin(longDistance/2);
    
    double valueWithRadius =  2 * atan2(sqrt(value), sqrt(1-value));
    
    return valueWithRadius * earthRadius;
}



/**
 * CONVERTION DATE
 * */

+(NSString*) convertDateToString:(NSDate*) date{
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:dateMask];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    return [dateFormatter stringFromDate:date];
    
}
+(NSDate*) convertStringToDate:(NSString*) stringDate{

    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:dateMask];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    return [dateFormatter dateFromString:stringDate];
    
}


/*
 * CHECK IF LAUNCHER APP IS FROM APP
 */

+(void) checkIfApplicationStartFromGeofencingNotification:(NSDictionary *)launchOptions {
    
    UILocalNotification *localNotif = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    if (localNotif) {
        
        NSDictionary* dictionary = localNotif.userInfo;
        
        int notificationId = [[dictionary objectForKey:notificationIdKey] intValue];
        int poiId = [[dictionary objectForKey:poiIdKey] intValue];
        
        AppPOINotification* poiNotification = [MobFirstGeoFencingStorage getNotification:notificationId andPOIId:poiId];
        if(poiNotification && poiNotification.showFullMessage){
            [MobFirstGeoFencingDialog showDialogWithMessage:[poiNotification getFullMessage]];
        }
    }
}


/*
 * HANDLER RECEIVE LOCAL NOTIFICATION
 */

+(void) onReceiveLocalNotification:(UIApplication *)application notification:(UILocalNotification *)notification{
    
    
    NSDictionary* dictionary = notification.userInfo;
    
    [application setApplicationIconBadgeNumber:1];
    [application setApplicationIconBadgeNumber:0];
    

    int notificationId = [[dictionary objectForKey:notificationIdKey] intValue];
    int poiId = [[dictionary objectForKey:poiIdKey] intValue];
    
    if ([application applicationState] == UIApplicationStateActive) {
        
        AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);
        
        AppPOINotification* poiNotification = [MobFirstGeoFencingStorage getNotification:notificationId andPOIId:poiId];
        if(poiNotification){
            NSString* message = poiNotification.shortMessage;
            if(poiNotification.showFullMessage && [poiNotification getFullMessage]){
                message = [poiNotification getFullMessage];
            }
            [MobFirstGeoFencingDialog showDialogWithMessage:message];
        }
    }else{
        
        /*
         * Storege To check if opp From notification
         */
        
        [MobFirstGeoFencingStorage saveNotificationToOpenApp:notificationId andPOIId:poiId];
        
    }
}



/*
 * HANDLER APP BECOME ACTIVE
 */

+(void) checkIfApplicationActiveFromGeofencingNotification{

    
    AppPOINotification* poiNotification = [MobFirstGeoFencingStorage getNotificationFromOpenApp];
    
    if(poiNotification){
        [MobFirstGeofencingConnection setUsageNotificationOpen:[NSString stringWithFormat:@"%d",poiNotification.id]];
        if(poiNotification.showFullMessage && [poiNotification getFullMessage]){
            [MobFirstGeoFencingDialog showDialogWithMessage:[poiNotification getFullMessage]];
        }
     
        [MobFirstGeoFencingStorage removeNotificationToOpenApp];
    }
}

@end
