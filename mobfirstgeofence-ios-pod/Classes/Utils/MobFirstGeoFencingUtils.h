//
//  MobFirstGeoFencingUtils.h
//  MobFirstGeoFencing
//
//  Created by Diego P Navarro on 18/10/16.
//  Copyright © 2016 Diego P Navarro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface MobFirstGeoFencingUtils : NSObject


/**
 *   HAVERSINE IMPLEMENTATION
 * */

+(double) convertGeoLocDistanceOnMeters:(double) latitude longitude:(double) longitude latitudeCompare:(double) latitudeCompare longitudeCompare:(double) longitudeCompare;

/**
 * CONVERTION DATE
 * */

+(NSString*) convertDateToString:(NSDate*) date;
+(NSDate*) convertStringToDate:(NSString*) stringDate;


/*
 * CHECK IF LAUNCHER APP IS FROM APP
 */

+(void) checkIfApplicationStartFromGeofencingNotification:(NSDictionary *)launchOptions;


/*
 * HANDLER RECEIVE LOCAL NOTIFICATION
 */

+(void) onReceiveLocalNotification:(UIApplication *)application notification:(UILocalNotification *)notification;


/*
 * HANDLER APP BECOME ACTIVE
 */

+(void) checkIfApplicationActiveFromGeofencingNotification;
@end
