//
//  MobFirstAPIConnection.m
//  MobFirstGeoFencing
//
//  Created by Diego P Navarro on 29/03/17.
//  Copyright © 2017 Diego P Navarro. All rights reserved.
//

#import "MobFirstAPIConnection.h"
#import "MobFirstCommunication.h"
#import "MobFirstAPIMapper.h"


@interface MobFirstAPIConnection(){
    BOOL isStagging;
}

@end

@implementation MobFirstAPIConnection

/*
 * Instance
 */

+(MobFirstAPIConnection*) getInstance{
    return [self getInstance:NO];
}
+(MobFirstAPIConnection*) getInstance:(BOOL) stagging{
    MobFirstAPIConnection* apiConnection = [MobFirstAPIConnection new];
    [apiConnection setStagging:stagging];
    return apiConnection;
}

/*
 * Set To API Stagging
 */

-(void)setStagging:(BOOL) stagging{
    isStagging = stagging;
}


/*
 * Auth By secret
 */


-(void) getConnectionAuthentication:(NSString*) clientId secret:(NSString*) secret block:(void (^)(NSString* token,NSError* error))block{
    NSDictionary* params =@{
                            MOBFIRST_HEADER_GRANT_TYPE:MOBFIRST_HEADER_CLIENT_CREDENTIALS,
                            MOBFIRST_HEADER_CLIENT_ID:clientId,
                            MOBFIRST_HEADER_CLIENT_SECRET:secret
                            };
    NSString* url = isStagging ? MOBFIRST_URL_BASE_STAGING : MOBFIRST_URL_BASE;
    
    url = [NSString stringWithFormat:@"%@/%@",url,MOBFIRST_COMPLEMENTION_OAUTH];
    
    [[MobFirstCommunication initWithConnectionType:ConnectionTypePost urlString:url path:nil parameters:params headers:nil changeSerializer:NO andResponseBlock:^(NSString *response, NSDictionary* headers ,NSError *error) {
        if(error){
            block(nil,error);
        }
        else{
            block([MobFirstAPIMapper getMobFistToken:response],nil);
        }
    }] connectionStart];
}



/*
 * Get Store Pois
 */

-(void) getPoiFromApp:(NSString*) appId apiToken:(NSString*) token block:(void (^)(NSArray* items,NSError* error))block{
    [self getPoiFromAppOnPage:1 appId:appId apiToken:token itemsData:[NSMutableArray new] block:block];
}


-(void) getPoiFromAppOnPage:(int) page appId:(NSString*) appId apiToken:(NSString*) apiToken itemsData:(NSMutableArray*) itemsData block:(void (^)(NSArray* items,NSError* error))block{
    
    
    NSDictionary* headers =@{MOBFIRST_HEADER_AUTHORIZATION: [NSString stringWithFormat:@"Bearer %@",apiToken]};
    
    NSString* url = isStagging ? MOBFIRST_URL_BASE_STAGING : MOBFIRST_URL_BASE;
    
    url = [NSString stringWithFormat:@"%@/apps/%@/features/geofences/poi?qnty=20&page=%d",url,appId,page];
    
    
    [[MobFirstCommunication initWithConnectionType:ConnectionTypeGet urlString:url path: nil headers:headers andResponseBlock:^(NSString * response , NSDictionary* headers, NSError * error) {
        if(error){
            block(nil,error);
        }else{
            
            id data = [MobFirstAPIMapper getPOIDataInformationFromAPIResponse:response];
            
            if(data){
                [itemsData addObjectsFromArray:data];
                [self getPoiFromAppOnPage:page + 1 appId:appId apiToken:apiToken itemsData:itemsData block:block];
            }
            else{
                block(itemsData,nil);
            }
        }
    }] connectionStart];
}



/*
 * Pois Events
 */

-(void) setPoiNotificationUsageOpen:(NSString*) clientId secret:(NSString*) secret appId:(NSString*) appId notifcationId:(NSString*) notifcationId block:(void (^)(BOOL success,NSError* error))block{
    
    
    [self getConnectionAuthentication:clientId secret:secret block:^(NSString *token, NSError *error) {
        
        if(error){
            block(NO,error);
        }
        else{
            
            [self setPoiNotificationUsage:token type:@"iOSOpeningCount" appId:appId notifcationId:notifcationId block:block];
        }
    }];
}

-(void) setPoiNotificationUsageTrigger:(NSString*) clientId secret:(NSString*) secret appId:(NSString*) appId notifcationId:(NSString*) notifcationId block:(void (^)(BOOL success,NSError* error))block{
    
    [self getConnectionAuthentication:clientId secret:secret block:^(NSString *token, NSError *error) {
        
        if(error){
            block(NO,error);
        }
        else{
            
            [self setPoiNotificationUsage:token type:@"iOSTriggerCount" appId:appId notifcationId:notifcationId block:block];
        }
    }];
    
}


-(void) setPoiNotificationUsage:(NSString*) token type:(NSString*) type appId:(NSString*) appId notifcationId:(NSString*) notificationId block:(void (^)(BOOL success,NSError* error))block{
    
        NSDictionary* headers =@{MOBFIRST_HEADER_AUTHORIZATION: [NSString stringWithFormat:@"Bearer %@",token]};
    
    NSDictionary* params =@{
                            @"Type":type,
                            @"Operation":@"Add"
                            };
    NSString* url = isStagging ? MOBFIRST_URL_BASE_STAGING : MOBFIRST_URL_BASE;
    
    url = [NSString stringWithFormat:@"%@/apps/%@/features/geofences/notifications/%@/usage",url,appId,notificationId];
    
    [[MobFirstCommunication initWithConnectionType:ConnectionTypePost urlString:url path:nil parameters:params headers:headers changeSerializer:NO andResponseBlock:^(NSString *response, NSDictionary* headers ,NSError *error) {
        if(error){
            block(NO,error);
        }
        else{
            block(YES,nil);
        }
    }] connectionStart];
}




@end

