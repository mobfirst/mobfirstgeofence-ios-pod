//
//  MobFirstGeofencingConnection.m
//  MobFirstGeoFencing
//
//  Created by Diego P Navarro on 18/10/16.
//  Copyright © 2016 Diego P Navarro. All rights reserved.
//

#import "MobFirstGeofencingConnection.h"
#import "MobFirstGeoFencingStorage.h"
#import "MobFirstAPIConnection.h"



#define MobFirstClientId @"MobFirstClientId"
#define MobFirstClientSecret @"MobFirstClientSecret"
#define MobFirstAppId @"MobFirstAppId"


@implementation MobFirstGeofencingConnection

/*
 * Update POIs
 */

+(void) startUpdatePOIs{
    
    [NSObject cancelPreviousPerformRequestsWithTarget: self selector:@selector(updatePOIs) object: self];
    [self updatePOIs];
}


+(void) updatePOIs{
    
    NSString* clientId = [[NSBundle mainBundle] objectForInfoDictionaryKey:MobFirstClientId];
    NSString* clientSecret = [[NSBundle mainBundle] objectForInfoDictionaryKey:MobFirstClientSecret];
    NSString* appId = [[NSBundle mainBundle] objectForInfoDictionaryKey:MobFirstAppId];
    
    
    if(!clientId || !clientSecret || !appId){
        return;
    }
    
    [[MobFirstAPIConnection getInstance] getConnectionAuthentication:clientId secret:clientSecret block:^(NSString *token, NSError *error) {
        
        if(!error && token){
            [[MobFirstAPIConnection getInstance] getPoiFromApp:appId apiToken:token block:^(NSArray* items, NSError *error) {
                if(!error){
                    [MobFirstGeoFencingStorage savePOIsFromAPI:items];
                }
            }];
        }
    }];
    
    /*
     * Wait 4 hours to update again
     */
    
   [self performSelector:@selector(updatePOIs) withObject: self afterDelay:  4 * 3600];
}


/*
 * POI Events
 */

+(void) setUsageNotificationTrigger:(NSString*) notificationId{
    
    NSString* clientId = [[NSBundle mainBundle] objectForInfoDictionaryKey:MobFirstClientId];
    NSString* clientSecret = [[NSBundle mainBundle] objectForInfoDictionaryKey:MobFirstClientSecret];
    NSString* appId = [[NSBundle mainBundle] objectForInfoDictionaryKey:MobFirstAppId];
    
    if(!clientId || !clientSecret || !appId){
        return;
    }
    
    [[MobFirstAPIConnection getInstance] setPoiNotificationUsageTrigger:clientId secret:clientSecret appId:appId notifcationId:notificationId block:^(BOOL success, NSError *error) {
    
    }];
    
    
}
+(void) setUsageNotificationOpen:(NSString*) notificationId{
    
    NSString* clientId = [[NSBundle mainBundle] objectForInfoDictionaryKey:MobFirstClientId];
    NSString* clientSecret = [[NSBundle mainBundle] objectForInfoDictionaryKey:MobFirstClientSecret];
    NSString* appId = [[NSBundle mainBundle] objectForInfoDictionaryKey:MobFirstAppId];
    
    if(!clientId || !clientSecret || !appId){
        return;
    }
    
    [[MobFirstAPIConnection getInstance] setPoiNotificationUsageOpen:clientId secret:clientSecret appId:appId notifcationId:notificationId block:^(BOOL success, NSError *error) {
        
    }];
    
}

@end
