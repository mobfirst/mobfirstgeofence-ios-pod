//
//  MobFirstCommunication.h
//  MobFirstGeoFencing
//
//  Created by Diego P Navarro on 29/03/17.
//  Copyright © 2017 Diego P Navarro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MobFirstCommunication : NSObject

typedef enum {
    ConnectionTypeGet,
    ConnectionTypePut,
    ConnectionTypePost,
    ConnectionTypeDelete,
} ConnectionType;


@property (nonatomic) ConnectionType connectionType;
@property (nonatomic, strong) NSString * url;
@property (nonatomic, strong) NSString * path;
@property (nonatomic, strong) NSDictionary * parameters;
@property (nonatomic, strong) NSDictionary * headers;
@property (nonatomic) BOOL  changeSerializer;
@property (nonatomic, strong) void(^responseBlock)(NSString *, NSDictionary* headers, NSError *);

+(MobFirstCommunication *) initWithConnectionType:(ConnectionType) connectionType urlString:(NSString*) url path:(NSString*) path headers:(NSDictionary*) headers andResponseBlock:(void (^)(NSString*,NSDictionary* headers,NSError*))block;

+(MobFirstCommunication *) initWithConnectionType:(ConnectionType) connectionType urlString:(NSString*) url path:(NSString*) path parameters:(NSDictionary*) parameters headers:(NSDictionary*) headers changeSerializer:(BOOL) changeSerializer andResponseBlock:(void (^)(NSString*,NSDictionary* headers,NSError*))block;


-(void) connectionStart;

@end
