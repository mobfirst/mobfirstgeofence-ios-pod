//
//  MobFirstCommunication.m
//  MobFirstGeoFencing
//
//  Created by Diego P Navarro on 29/03/17.
//  Copyright © 2017 Diego P Navarro. All rights reserved.
//

#import "MobFirstCommunication.h"
#import <AFNetworking/AFNetworking.h>

@implementation MobFirstCommunication


+(MobFirstCommunication *) initWithConnectionType:(ConnectionType) connectionType urlString:(NSString*) url path:(NSString*) path headers:(NSDictionary*) headers andResponseBlock:(void (^)(NSString*,NSDictionary*,NSError*))block{
    
    MobFirstCommunication * connection = [MobFirstCommunication new];
    connection.connectionType = connectionType;
    connection.url = url;
    connection.path = path;
    connection.responseBlock = block;
    connection.headers = headers;
    return connection;
}

+(MobFirstCommunication *) initWithConnectionType:(ConnectionType) connectionType urlString:(NSString*) url path:(NSString*) path parameters:(NSDictionary*) parameters headers:(NSDictionary*) headers changeSerializer:(BOOL) changeSerializer andResponseBlock:(void (^)(NSString*,NSDictionary* headers,NSError*))block
{
    MobFirstCommunication * connection = [MobFirstCommunication new];
    connection.connectionType = connectionType;
    connection.url = url;
    connection.path = path;
    connection.responseBlock = block;
    connection.headers = headers;
    connection.parameters = parameters;
    connection.changeSerializer = changeSerializer;
    return connection;
}


-(void) connectionStart{
    
    switch (self.connectionType) {
        case ConnectionTypeDelete:
            [self sendRequestDelete];
            break;
        case ConnectionTypePost:
            [self sendRequestPost];
            break;
        case ConnectionTypePut:
            [self sendRequestPut];
            break;
        case ConnectionTypeGet:
            [self sendRequestGet];
            break;
        default:
            break;
    }
}


-(AFHTTPRequestOperationManager*) setAFHTTPRequestOperationManager
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    AFHTTPResponseSerializer *serializer = [AFHTTPResponseSerializer serializer];
    
    if(self.changeSerializer){
        serializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
    }
    
    manager.responseSerializer = serializer;
    if(self.headers){
        NSArray* keys = [self.headers allKeys];
        for (NSString*  key in keys) {
            [manager.requestSerializer setValue:[self.headers valueForKey:key] forHTTPHeaderField:key];
        }
    }
    return manager;
}


-(void) sendRequestGet{
    [[self setAFHTTPRequestOperationManager] GET:self.url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *dictionary = nil;
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)operation.response;
        if ([httpResponse respondsToSelector:@selector(allHeaderFields)]) {
            dictionary = [httpResponse allHeaderFields];
        }
        if(self.responseBlock){
            self.responseBlock(operation.responseString,dictionary,nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSDictionary *dictionary = nil;
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)operation.response;
        if ([httpResponse respondsToSelector:@selector(allHeaderFields)]) {
            dictionary = [httpResponse allHeaderFields];
        }
        
        if(self.responseBlock){
            self.responseBlock(operation.responseString,dictionary,error);
        }
    }];
}

-(void) sendRequestPut{
    
    [[self setAFHTTPRequestOperationManager]  PUT:self.url  parameters:self.parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *dictionary = nil;
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)operation.response;
        if ([httpResponse respondsToSelector:@selector(allHeaderFields)]) {
            dictionary = [httpResponse allHeaderFields];
        }
        if(self.responseBlock){
            self.responseBlock(operation.responseString,dictionary,nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSDictionary *dictionary = nil;
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)operation.response;
        if ([httpResponse respondsToSelector:@selector(allHeaderFields)]) {
            dictionary = [httpResponse allHeaderFields];
        }
        
        if(self.responseBlock){
            self.responseBlock(operation.responseString,dictionary,error);
        }
    }];
}

-(void) sendRequestPost{
    
    [[self setAFHTTPRequestOperationManager]  POST:self.url  parameters:self.parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *dictionary = nil;
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)operation.response;
        if ([httpResponse respondsToSelector:@selector(allHeaderFields)]) {
            dictionary = [httpResponse allHeaderFields];
        }
        if(self.responseBlock){
            self.responseBlock(operation.responseString,dictionary,nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSDictionary *dictionary = nil;
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)operation.response;
        if ([httpResponse respondsToSelector:@selector(allHeaderFields)]) {
            dictionary = [httpResponse allHeaderFields];
        }
        
        if(self.responseBlock){
            self.responseBlock(operation.responseString,dictionary,error);
        }
    }];
}

-(void) sendRequestDelete{
    [[self setAFHTTPRequestOperationManager]  POST:self.url  parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *dictionary = nil;
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)operation.response;
        if ([httpResponse respondsToSelector:@selector(allHeaderFields)]) {
            dictionary = [httpResponse allHeaderFields];
        }
        if(self.responseBlock){
            self.responseBlock(operation.responseString,dictionary,nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSDictionary *dictionary = nil;
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)operation.response;
        if ([httpResponse respondsToSelector:@selector(allHeaderFields)]) {
            dictionary = [httpResponse allHeaderFields];
        }
        
        if(self.responseBlock){
            self.responseBlock(operation.responseString,dictionary,error);
        }
    }];
}



@end
