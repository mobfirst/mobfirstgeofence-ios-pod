//
//  MobFirstGeofencingConnection.h
//  MobFirstGeoFencing
//
//  Created by Diego P Navarro on 18/10/16.
//  Copyright © 2016 Diego P Navarro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MobFirstGeofencingConnection : NSObject


/*
 * Update POIs
 */

+(void) startUpdatePOIs;



/*
 * POI Events
 */

+(void) setUsageNotificationTrigger:(NSString*) notificationId;
+(void) setUsageNotificationOpen:(NSString*) notificationId;
@end
