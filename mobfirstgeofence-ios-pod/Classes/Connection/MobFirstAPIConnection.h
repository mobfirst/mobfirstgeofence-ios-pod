//
//  MobFirstAPIConnection.h
//  MobFirstGeoFencing
//
//  Created by Diego P Navarro on 29/03/17.
//  Copyright © 2017 Diego P Navarro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MobFirstAPIConstants.h"

@interface MobFirstAPIConnection : NSObject

/*
 * Instance
 */

+(MobFirstAPIConnection*) getInstance;
+(MobFirstAPIConnection*) getInstance:(BOOL) stagging;


/*
 * Set To API Stagging
 */


-(void)setStagging:(BOOL) stagging;

/*
 * Auth By secret
 */

-(void) getConnectionAuthentication:(NSString*) clientId secret:(NSString*) secret block:(void (^)(NSString* token,NSError* error))block;


/*
 * Get Store Pois
 */

-(void) getPoiFromApp:(NSString*) appId apiToken:(NSString*) token block:(void (^)(NSArray* items,NSError* error))block;


/*
 * Pois Events
 */

-(void) setPoiNotificationUsageOpen:(NSString*) clientId secret:(NSString*) secret appId:(NSString*) appId notifcationId:(NSString*) notifcationId block:(void (^)(BOOL success,NSError* error))block;
-(void) setPoiNotificationUsageTrigger:(NSString*) clientId secret:(NSString*) secret appId:(NSString*) appId notifcationId:(NSString*) notifcationId block:(void (^)(BOOL success,NSError* error))block;



@end
