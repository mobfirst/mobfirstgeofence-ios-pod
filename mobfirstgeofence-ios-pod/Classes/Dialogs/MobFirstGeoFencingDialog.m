//
//  MobFirstGeoFencingDialog.m
//  MobFirstGeoFencing
//
//  Created by Diego P Navarro on 21/10/16.
//  Copyright © 2016 Diego P Navarro. All rights reserved.
//

#import "MobFirstGeoFencingDialog.h"

@implementation MobFirstGeoFencingDialog

/*
* SHOW DIALOG
*/


+(void) showDialogWithMessage:(NSString*) message{
    
    
    
    
    
    
    [[[UIAlertView alloc]initWithTitle:[[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString*)kCFBundleNameKey]
                               message:message
                              delegate:nil
                     cancelButtonTitle:@"Ok"
                     otherButtonTitles:nil, nil]show];
    
}


@end
