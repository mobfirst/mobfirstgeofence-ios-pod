//
//  MobFirstGeoFencingDialog.h
//  MobFirstGeoFencing
//
//  Created by Diego P Navarro on 21/10/16.
//  Copyright © 2016 Diego P Navarro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface MobFirstGeoFencingDialog : NSObject


/*
 * SHOW DIALOG
 */


+(void) showDialogWithMessage:(NSString*) message ;

@end
