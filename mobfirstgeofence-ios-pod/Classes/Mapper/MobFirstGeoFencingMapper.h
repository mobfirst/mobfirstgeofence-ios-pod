//
//  MobFirstGeoFencingMapper.h
//  MobFirstGeoFencing
//
//  Created by Diego P Navarro on 18/10/16.
//  Copyright © 2016 Diego P Navarro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MobFirstAPIMapper.h"

@interface MobFirstGeoFencingMapper : MobFirstAPIMapper


/*
 * Mapper Object from API Dictionary
 */


+(NSArray*) convertAPIResponseDictionaryToObjects:(NSArray*) listDictionary;



@end
