//
//  MobFirstGeoFencingMapper.m
//  MobFirstGeoFencing
//
//  Created by Diego P Navarro on 18/10/16.
//  Copyright © 2016 Diego P Navarro. All rights reserved.
//

#import "MobFirstGeoFencingMapper.h"
#import "AppPOI.h"
#import "AppPOINotification.h"


@implementation MobFirstGeoFencingMapper


/*
 * Mapper Object from API Dictionary
 */


+(NSArray*) convertAPIResponseDictionaryToObjects:(NSArray*) listDictionary{
    
    if(!listDictionary){
        return nil;
    }
    
    NSArray* poisList = [NSArray new];
    
    for(id item in listDictionary){
        poisList  = [poisList arrayByAddingObject:[self getPOIFromdictionary:item]];
    }
    
    return poisList;
}

+(AppPOI*) getPOIFromdictionary:(id) dictionary{
    
    AppPOI* poi = [AppPOI new];
    
    poi.id = [[self getValue:@"Id" fromDictionary:dictionary] intValue];
    poi.latitude = [[self getValue:@"Latitude" fromDictionary:dictionary] doubleValue];
    poi.longitude = [[self getValue:@"Longitude" fromDictionary:dictionary] doubleValue];
    poi.title = [self getValue:@"Title" fromDictionary:dictionary] ;
    poi.radius = [[self getValue:@"Radius" fromDictionary:dictionary] intValue];
    
    id notifications = [self getValue:@"Notifications" fromDictionary:dictionary];
    
    
    NSMutableArray* notificationObjectList = [NSMutableArray new];
    
    if(notifications && [notifications isKindOfClass:[NSArray class]]){
        for(id itemNotification in notifications){
            
            AppPOINotification* notification = [self getNotificationFromDictionary:itemNotification];
            
            notification.poiId = poi.id;
            notification.latitude = poi.latitude;
            notification.longitude = poi.longitude;
            notification.radius = poi.radius;
            
            [notificationObjectList addObject: notification];
        }
    }
    
    poi.notifications = notificationObjectList;
    poi.objectAPI = dictionary;
    return poi;
}


+(AppPOINotification*) getNotificationFromDictionary:(id) dictionary{
    
    AppPOINotification* notification = [AppPOINotification new];
    
    notification.active = [[self getValue:@"Active" fromDictionary:dictionary] intValue] == 1;
    notification.endDate = [self getValue:@"EndDate" fromDictionary:dictionary];
    notification.fullMessage = [self getValue:@"FullMessage" fromDictionary:dictionary];
    notification.hasTriggerPeriod = [[self getValue:@"HasTriggerPeriod" fromDictionary:dictionary] intValue] == 1;
    notification.id = [[self getValue:@"Id" fromDictionary:dictionary] intValue];
    notification.shortMessage = [self getValue:@"ShortMessage" fromDictionary:dictionary];
    notification.showFullMessage = [[self getValue:@"ShowFullMessage" fromDictionary:dictionary] intValue] == 1;
    notification.startDate = [self getValue:@"StartDate" fromDictionary:dictionary];
    notification.triggerInterval = [[self getValue:@"TriggerInterval" fromDictionary:dictionary] intValue];
    notification.triggerPeriodEndtTime = [self getValue:@"TriggerPeriodEndTime" fromDictionary:dictionary];
    notification.triggerPeriodStartTime = [self getValue:@"TriggerPeriodStartTime" fromDictionary:dictionary];
    
    notification.objectAPI = dictionary;
    
    return notification;

}

@end
