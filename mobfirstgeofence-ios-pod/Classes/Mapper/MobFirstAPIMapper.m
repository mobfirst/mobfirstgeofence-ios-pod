//
//  MobFirstAPIMapper.m
//  MobFirstGeoFencing
//
//  Created by Diego P Navarro on 29/03/17.
//  Copyright © 2017 Diego P Navarro. All rights reserved.
//

#import "MobFirstAPIMapper.h"

@implementation MobFirstAPIMapper


/*
 * Helpers Mappers Methods
 */

+(NSDictionary*) convertNSStringToDictionary:(NSString*) string{
    NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    return json;
}

+(id) checkIfInformationIsNil:(id) information{
    return [information isKindOfClass:[NSNull class]] ? nil : information;
}

+(id) getValue:(NSString*) value fromDictionary:(id) dictionary{
    id data = [dictionary objectForKey:value];
    return [self checkIfInformationIsNil:data];
}


/*
 * Token
 */

+(NSString*) getMobFistToken:(NSString*) apiResponse{
    NSDictionary * json = [self convertNSStringToDictionary:apiResponse];
    if(!json){
        return nil;
    }
    return [self getValue:@"access_token" fromDictionary:json];
}



/*
 * Get Data From API Response
 */

+(id) getPOIDataInformationFromAPIResponse:(NSString*) apiResponse{
    
    NSDictionary* responseDic = [self convertNSStringToDictionary:apiResponse];
    
    if(!responseDic){
        return nil;
    }
    
    id data = [self getValue:@"Data" fromDictionary:responseDic];
    
    if(!data || ![data isKindOfClass:[NSArray class]]){
        return nil;
    }
    
    NSArray* dataArray = (NSArray*) data;
    return [dataArray count] > 0 ? data: nil;
}



@end
