//
//  MobFirstAPIMapper.h
//  MobFirstGeoFencing
//
//  Created by Diego P Navarro on 29/03/17.
//  Copyright © 2017 Diego P Navarro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MobFirstAPIMapper : NSObject

/*
 * Helpers Mappers Methods
 */

+(id) checkIfInformationIsNil:(id) information;
+(id) getValue:(NSString*) value fromDictionary:(id) dictionary;

/*
 * Token
 */

+(NSString*) getMobFistToken:(NSString*) apiResponse;

/*
 * Get Data From API Response
 */

+(id) getPOIDataInformationFromAPIResponse:(NSString*) apiResponse;

@end
