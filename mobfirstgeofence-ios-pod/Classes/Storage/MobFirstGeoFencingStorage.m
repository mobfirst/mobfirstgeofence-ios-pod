//
//  MobFirstGeoFencingStorage.m
//  MobFirstGeoFencing
//
//  Created by Diego P Navarro on 18/10/16.
//  Copyright © 2016 Diego P Navarro. All rights reserved.
//

#import "MobFirstGeoFencingStorage.h"
#import "MobFirstGeoFencingMapper.h"
#import "MobFirstGeoFencingUtils.h"

#define POIsSerializeds @"POIsSerializeds"
#define POIsItem @"POIsItem"



@implementation MobFirstGeoFencingStorage


/**
 *  POIs FROM API
 * */

+(BOOL) savePOIsFromAPI:(NSArray*) pois{
    NSData* data =  [NSKeyedArchiver archivedDataWithRootObject:pois];
    return [self saveItemOnUserDefaults:data andKey:POIsSerializeds];
}

+(NSArray*) getPois{
    
    NSData* data = [self getItemFromUserDefaults:POIsSerializeds];
    
    if(!data){
        return nil;
    }

    return [MobFirstGeoFencingMapper convertAPIResponseDictionaryToObjects :[NSKeyedUnarchiver unarchiveObjectWithData:data] ];  
}


/**
 * NOTIFICATIONS
 */
+(BOOL) saveNotification:(AppPOINotification*) notification andDateTrigger:(NSDate*) date{
    
    
    notification.lastTriggerDate = [MobFirstGeoFencingUtils convertDateToString:date];
    
    
    NSArray* itemsNotification = [self getAllPoisSaved];
    
    
    if([self getNotification:notification.id andPOIId:notification.poiId]){
        
        /*
         * Already exist the notification, so update
         */
        
        
        for(AppPOINotification* notificationSaved in itemsNotification){
            if(notificationSaved.id == notification.id && notificationSaved.poiId == notification.poiId){
                notificationSaved.lastTriggerDate = notification.lastTriggerDate;
            }
        }
    }
    else{
        
        
        /*
         * add notification
         */
        
        
        itemsNotification = [itemsNotification arrayByAddingObject:notification];
    }
    
    return [self saveAllNotifications:itemsNotification];
   
}



+(NSArray*) getAllPoisSaved{
    
    
    NSDictionary* savedPois = [self getItemDictionaryFromUserDefaults:POIsItem];
    
    if(!savedPois){
        return nil;
    }
    
    
    NSMutableArray* listNotification = [NSMutableArray new];
    
    for(NSString* key in [savedPois allKeys]){
        
        NSDictionary* itemDictionaryPOI = [savedPois objectForKey:key];
        
        @try {
            
            AppPOINotification* notification = [AppPOINotification new];
            
            notification.id = [[itemDictionaryPOI objectForKey:notificationIdKey] intValue];
            notification.poiId = [[itemDictionaryPOI objectForKey:poiIdKey] intValue];
            notification.lastTriggerDate = [itemDictionaryPOI objectForKey:triggerDateKey];
            notification.showFullMessage = [[itemDictionaryPOI objectForKey:showFullMessageKey] boolValue];
            notification.fullMessage = [itemDictionaryPOI objectForKey:fullMessageKey];
            notification.shortMessage = [itemDictionaryPOI objectForKey:shortMessageKey];
            
            [listNotification addObject:notification];
        }
        @catch (NSException *exception) {}
        @finally {}
    }

    return listNotification;
    
}


+(BOOL) saveAllNotifications:(NSArray*) listNotification{
    
    @try {
        
        NSMutableDictionary* dictionaryItems = [NSMutableDictionary new];

        for(int count = 0; count < [listNotification count] ; count++){
            
            AppPOINotification* notification = listNotification[count];
            
            [dictionaryItems addEntriesFromDictionary: @{ [NSNumber numberWithInt:count]:@{
                                                                  notificationIdKey : [NSNumber numberWithInt:notification.id],
                                                                  poiIdKey : [NSNumber numberWithInt: notification.poiId],
                                                                  triggerDateKey :  notification.lastTriggerDate,
                                                                  showFullMessageKey :  [NSNumber numberWithBool: notification.showFullMessage],
                                                                  fullMessageKey : [notification getFullMessage],
                                                                  shortMessageKey : notification.shortMessage
                                                                  },
                                                          }];
            
        }

        return [self saveDictionaryOnUserDefaults:dictionaryItems andKey:POIsItem];
            
    } @catch (NSException *exception) {
        return NO;
    } @finally {}
}


+(AppPOINotification*) getNotification:(int) id andPOIId:(int) poiId{
    
    NSArray* itemsNotification = [self getAllPoisSaved];
    
    for(AppPOINotification* notification in itemsNotification){
        if(notification.id == id && notification.poiId == poiId){
            return notification;
        }
    }
    
    return nil;
}


/*
 * Save notification to open App
 */

+(BOOL) saveNotificationToOpenApp:(int) id andPOIId:(int) poiId{
    
    return [self saveItemOnUserDefaults:[NSString stringWithFormat:@"%d",id] andKey:@"openAppNotId"] &&
                [self saveItemOnUserDefaults:[NSString stringWithFormat:@"%d",poiId] andKey:@"openAppPoiId"];
    
}

+(BOOL) removeNotificationToOpenApp{
    return [ self removeItemOnUserDefaults:@"openAppNotId"] && [self removeItemOnUserDefaults:@"openAppPoiId"];
}

+(AppPOINotification*) getNotificationFromOpenApp{
    
    NSString* notId = [self getItemFromUserDefaults:@"openAppNotId"];
    NSString* poiId = [self getItemFromUserDefaults:@"openAppPoiId"];
    
    if(!notId || !poiId){
        return  nil;
    }
    
    return [self getNotification:[notId intValue] andPOIId:[poiId intValue]];
}



/*
 * UserDefaults
 */

+(BOOL) saveItemOnUserDefaults:(id) item andKey:(NSString*) key{
    
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:item forKey:key];
    return [defaults synchronize];
}

+(id) getItemFromUserDefaults:(NSString*) key{
    
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    return [defaults objectForKey:key];
}


+(BOOL) removeItemOnUserDefaults:(NSString*) key{
    
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:key];
    return [defaults synchronize];
}


+(BOOL) saveDictionaryOnUserDefaults:(id) item andKey:(NSString*) key{
    NSData* data =  [NSKeyedArchiver archivedDataWithRootObject:item];
    return [self saveItemOnUserDefaults:data andKey:key];
}

+(id) getItemDictionaryFromUserDefaults:(NSString*) key{
    NSData* data = [self getItemFromUserDefaults:key];
    if(!data){
        return nil;
    }
    return [NSKeyedUnarchiver unarchiveObjectWithData:data];
}




@end
