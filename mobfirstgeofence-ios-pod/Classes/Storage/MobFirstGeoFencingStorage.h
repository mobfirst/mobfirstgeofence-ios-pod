//
//  MobFirstGeoFencingStorage.h
//  MobFirstGeoFencing
//
//  Created by Diego P Navarro on 18/10/16.
//  Copyright © 2016 Diego P Navarro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppPOINotification.h"


/*
 * Dictionary items keys
 */
#define shortMessageKey @"shortMessageKey"
#define fullMessageKey @"fullMessageKey"
#define showFullMessageKey @"showFullMessageKey"
#define triggerDateKey @"triggerDateKey"
#define notificationIdKey @"notificationIdKey"
#define poiIdKey @"poiIdKey"


@interface MobFirstGeoFencingStorage : NSObject


/**
 *  POIs FROM API
 * */

+(BOOL) savePOIsFromAPI:(NSArray*) pois;
+(NSArray*) getPois;


/**
 * NOTIFICATIONS
 */
+(BOOL) saveNotification:(AppPOINotification*) notification andDateTrigger:(NSDate*) date;
+(AppPOINotification*) getNotification:(int) id andPOIId:(int) poiId;



/*
 * Save notification to open App
 */

+(BOOL) saveNotificationToOpenApp:(int) id andPOIId:(int) poiId;
+(BOOL) removeNotificationToOpenApp;
+(AppPOINotification*) getNotificationFromOpenApp;


@end
