# mobfirstgeofence-ios-pod

[![CI Status](http://img.shields.io/travis/Diego Merks/mobfirstgeofence-ios-pod.svg?style=flat)](https://travis-ci.org/Diego Merks/mobfirstgeofence-ios-pod)
[![Version](https://img.shields.io/cocoapods/v/mobfirstgeofence-ios-pod.svg?style=flat)](http://cocoapods.org/pods/mobfirstgeofence-ios-pod)
[![License](https://img.shields.io/cocoapods/l/mobfirstgeofence-ios-pod.svg?style=flat)](http://cocoapods.org/pods/mobfirstgeofence-ios-pod)
[![Platform](https://img.shields.io/cocoapods/p/mobfirstgeofence-ios-pod.svg?style=flat)](http://cocoapods.org/pods/mobfirstgeofence-ios-pod)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

mobfirstgeofence-ios-pod is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "mobfirstgeofence-ios-pod"
```

## Author

Diego Merks, merks@mobfirst.com

## License

mobfirstgeofence-ios-pod is available under the MIT license. See the LICENSE file for more info.
